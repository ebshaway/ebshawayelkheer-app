package com.ebshaway.elkheer.helpers

import android.graphics.Color
import android.view.View
import android.widget.TextView
import com.ebshaway.elkheer.R
import com.google.android.material.snackbar.Snackbar


fun showSnack(context: View, text: CharSequence) {
    val mSnackbar = Snackbar.make(
        context,
        text,
        Snackbar.LENGTH_LONG
    )
    val mView: View = mSnackbar.view
    val mTextView =
        mView.findViewById<View>(R.id.snackbar_text) as TextView

    mTextView.textAlignment =
        View.TEXT_ALIGNMENT_CENTER

    mTextView.setTextColor(Color.RED)
    mSnackbar.show()
}
