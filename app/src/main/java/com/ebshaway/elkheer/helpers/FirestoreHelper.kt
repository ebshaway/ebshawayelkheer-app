package com.ebshaway.elkheer.helpers

import android.util.Log
import com.ebshaway.elkheer.model.ReportModel
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.SetOptions
import com.google.gson.Gson
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


object FirestoreHelper {
    private val db = FirebaseFirestore.getInstance()
    private const val TAG: String = "FireStoreHelper"
    private const val groups: String = "Groups"
    private const val people: String = "People"

    fun getGroups(listner: getGroups) {
        db.collection(groups).get()
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val list: MutableList<String> = ArrayList()
                    for (document in task.result!!) {
//                        list.add(document.id)
                        listner.onGetGroups(document.id)

                    }
                } else {
                    Log.d(TAG, "Error getting documents: ", task.exception)
                }
            }
    }

    fun getGroupDetails(id: String, listener: getDetails) {
        db.collection(groups).document(id)
            .addSnapshotListener { snapshot, e ->
                if (e != null) {
                    Log.w(TAG, "Listen failed.", e)
                    return@addSnapshotListener
                }
                if (snapshot != null && snapshot.exists()) {
                    listener.onDetails(
                        Gson().toJson(snapshot.data!!)
                    )
                } else {
                    Log.d(TAG, "Current data: null")
                }
            }
    }

    fun savePeople(report: ReportModel) {
        val savereport = hashMapOf(
            (System.currentTimeMillis() / 1000).toString() + report.name to report
        )
        db.collection(people).document(report.type)
            .set(savereport, SetOptions.merge())
    }

    interface getGroups {
        fun onGetGroups(
            groups: String
        )
    }

    interface getDetails {
        fun onDetails(data: String)
    }
}