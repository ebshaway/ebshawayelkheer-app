package com.ebshaway.elkheer.ui.centers

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.ebshaway.elkheer.R
import com.ebshaway.elkheer.helpers.showSnack
import com.ebshaway.elkheer.model.Place
import com.google.gson.Gson
import kotlinx.android.synthetic.main.app_bar.*
import kotlinx.android.synthetic.main.fragment_center_details.*

class CenterDetailsFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_center_details, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val place = Gson().fromJson(arguments?.getString("place"), Place::class.java)
        activity_name.text = place.name
        back.setOnClickListener {
            view?.findNavController()?.navigate(
                R.id.action_center_to_places, bundleOf("groupId" to arguments?.getInt("groupId"))
            )
        }
        center_name.text = place.name
        center_address.text = place.address
        if (place.phone?.isNotEmpty()!!) {
            center_phone.text = place.phone

        } else {
            center_phone.text = resources.getString(R.string.no_phone_available)
        }
        call.setOnClickListener {
            if (place.phone!!.isNotEmpty()) {
                val intent = Intent(Intent.ACTION_DIAL)
                intent.data = Uri.parse("tel:${place.phone}")
                activity?.startActivity(intent)
            } else {
                showSnack(it, resources.getText(R.string.no_phone_available))

            }

        }

        whatsapp.setOnClickListener {
            if (place.whatsapp?.isNotEmpty()!!) {
                val url = "https://api.whatsapp.com/send?phone=${place.whatsapp}"
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(url)
                startActivity(i)
            } else {
                showSnack(it, resources.getText(R.string.no_phone_available))
            }
        }
        when (place.type) {
            1 -> {
                details_image.setImageDrawable(resources.getDrawable(R.drawable.ic_book))
            }
            2 -> {
                details_image.setImageDrawable(resources.getDrawable(R.drawable.ic_phone))
            }
            3 -> {
                details_image.setImageDrawable(resources.getDrawable(R.drawable.ic_market))
            }
            4 -> {
                details_image.setImageDrawable(resources.getDrawable(R.drawable.ic_user))
            }
            5 -> {
                details_image.setImageDrawable(resources.getDrawable(R.drawable.ic_laptop))
            }
            6 -> {
                details_image.setImageDrawable(resources.getDrawable(R.drawable.ic_pharmacy))
            }
            7 -> {
                details_image.setImageDrawable(resources.getDrawable(R.drawable.ic_home))
            }
        }
    }
}
