package com.ebshaway.elkheer.ui.fonts


import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.view.View
import com.ebshaway.elkheer.ui.fonts.FontsHelper


class CustomTextView : androidx.appcompat.widget.AppCompatTextView {
    constructor(context: Context) : super(context) {
        setView()

    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        setView()


    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        setView()
    }

    fun setView() {
        this.typeface = FontsHelper.regTypeface(this.context)
        this.textDirection = View.TEXT_DIRECTION_ANY_RTL

    }
}

