package com.ebshaway.elkheer.ui.fonts

import android.content.Context
import android.graphics.Typeface
import com.ebshaway.elkheer.R


object FontsHelper {
    fun regTypeface(context: Context): Typeface {
        return Typeface.createFromAsset(context.assets, context.getString(R.string.font_light))
    }
}
