package com.ebshaway.elkheer.ui.report

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.core.os.bundleOf
import androidx.navigation.findNavController

import com.ebshaway.elkheer.R
import com.ebshaway.elkheer.helpers.FirestoreHelper
import com.ebshaway.elkheer.helpers.showSnack
import com.ebshaway.elkheer.model.ReportModel
import kotlinx.android.synthetic.main.app_bar.back
import kotlinx.android.synthetic.main.fragment_report.*

class ReportFragment : Fragment() {
    private var selectedStatus: String = ""
    private var selectedType: String = ""
    private var stauts = ArrayList<String>()
    private var types = ArrayList<String>()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_report, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        back.setOnClickListener {
            view.findNavController().navigate(
                R.id.action_report_to_main
            )
        }
        stauts.add("الحالة الاجتماعية")
        stauts.add("اعزب")
        stauts.add("متزوج")
        stauts.add("ارمل")
        stauts.add("مطلق")

        types.add("اختيار نوع الحالة")
        types.add("مرض")
        types.add("فقر")
        types.add("دين")
        types.add("اخرى")
        handleStatusSpinner(stauts)
        handleTypeSpinner(types)
        add.setOnClickListener {
            if (name_editText.text.toString().isNotEmpty()) {
                FirestoreHelper.savePeople(
                    ReportModel(
                        name_editText.text.toString(),
                        address_editText.text.toString(),
                        phone_editText.text.toString(),
                        selectedStatus,
                        selectedType,
                        message_editText.text.toString()
                    )
                )
                showSnack(it, resources.getText(R.string.done))
                view.findNavController().navigate(
                    R.id.action_report_to_main
                )

            } else {
                showSnack(it, resources.getText(R.string.invaild_name))
            }
        }
    }

    private fun handleStatusSpinner(status: ArrayList<String>) {
        val adapter = context?.let {
            ArrayAdapter(
                it,
                R.layout.time_slopt_spinner_item, status
            )
        }
        adapter?.setDropDownViewResource(R.layout.time_slopt_spinner_item)
        status_spinner.adapter = adapter
        status_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                selectedStatus = parent.adapter.getItem(position) as String
                Log.d("SelectedMonth ", position.toString())
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
            }
        }
    }

    private fun handleTypeSpinner(status: ArrayList<String>) {
        val adapter = context?.let {
            ArrayAdapter(
                it,
                R.layout.time_slopt_spinner_item, status
            )
        }
        adapter?.setDropDownViewResource(R.layout.time_slopt_spinner_item)
        type_spinner.adapter = adapter
        type_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                selectedType = parent.adapter.getItem(position) as String
                Log.d("SelectedMonth ", position.toString())
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
            }
        }
    }
}
