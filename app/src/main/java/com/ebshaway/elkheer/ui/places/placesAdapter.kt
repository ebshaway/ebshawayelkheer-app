package com.ebshaway.elkheer.ui.places

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.ebshaway.elkheer.R
import com.ebshaway.elkheer.model.Place
import com.squareup.picasso.Picasso

class placesAdapter(
    private var runSheetModels: ArrayList<Place>,
    private var listener: itemClicked
) :
    RecyclerView.Adapter<placesAdapter.ViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.places_item, parent, false)

        return ViewHolder(itemView)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = runSheetModels[position]

        holder.container?.setOnClickListener {
            listener.onClick(item)
        }

        holder.name?.text = item.name

        when (item.type) {
            1 -> {
                holder.icon?.setImageDrawable(holder.itemView.context.resources.getDrawable(R.drawable.ic_book))
            }
            2 -> {
                holder.icon?.setImageDrawable(holder.itemView.context.resources.getDrawable(R.drawable.ic_phone))
            }
            3 -> {
                holder.icon?.setImageDrawable(holder.itemView.context.resources.getDrawable(R.drawable.ic_market))
            }
            4 -> {
                holder.icon?.setImageDrawable(holder.itemView.context.resources.getDrawable(R.drawable.ic_user))
            }
            5 -> {
                holder.icon?.setImageDrawable(holder.itemView.context.resources.getDrawable(R.drawable.ic_laptop))
            }
            6 -> {
                holder.icon?.setImageDrawable(holder.itemView.context.resources.getDrawable(R.drawable.ic_pharmacy))
            }
            7 -> {
                holder.icon?.setImageDrawable(holder.itemView.context.resources.getDrawable(R.drawable.ic_home))
            }
        }
    }

    override fun getItemCount(): Int {
        return runSheetModels.size
    }


    inner class ViewHolder(convertView: View) : RecyclerView.ViewHolder(convertView) {
        //group
        var name: TextView? = convertView.findViewById(R.id.place_name)
        var icon: ImageView? = convertView.findViewById(R.id.place_icon)
        var container: CardView? = convertView.findViewById(R.id.container)

    }

    interface itemClicked {
        fun onClick(item: Place)
    }
}