package com.ebshaway.elkheer.ui.groups

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager

import com.ebshaway.elkheer.R
import com.ebshaway.elkheer.helpers.FirestoreHelper
import com.ebshaway.elkheer.model.Group
import kotlinx.android.synthetic.main.app_bar.*
import kotlinx.android.synthetic.main.fragment_groups.*

class GroupsFragment : Fragment(), GroupsAdapter.itemClicked {
    var adapter: GroupsAdapter? = null
    private var groupItems: ArrayList<Group> = ArrayList()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_groups, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity_name.text = resources.getString(R.string.groups)
        val linearLayoutManager = LinearLayoutManager(context)
        group_recycler.layoutManager = linearLayoutManager
        group_recycler.setHasFixedSize(true)
        adapter =
            GroupsAdapter(groupItems, this)
        group_recycler.adapter = adapter
        groupItems.clear()

        groupItems.add(Group(1, resources.getString(R.string.ebshawayelkheer), R.drawable.elkheer))
        groupItems.add(Group(2, resources.getString(R.string.mogtreeb), R.drawable.mogtreeb))
        groupItems.add(Group(3, resources.getString(R.string.halat), R.drawable.halat))
        groupItems.add(Group(4, resources.getString(R.string.gamaia), R.drawable.sharaia))
        groupItems.add(Group(5, resources.getString(R.string.banat), R.drawable.elkheer))

//        categoryItems.addAll(categoryItems)
        adapter!!.notifyDataSetChanged()
        back.setOnClickListener {
            view.findNavController().navigate(
                R.id.action_groups_to_main
            )
        }
    }

    override fun onClick(item: Group) {
        view?.findNavController()?.navigate(
            R.id.action_groups_to_places, bundleOf("groupId" to item.id)
        )
    }
}
