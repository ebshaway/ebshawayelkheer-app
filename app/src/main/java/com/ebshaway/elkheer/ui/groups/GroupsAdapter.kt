package com.ebshaway.elkheer.ui.groups

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.ebshaway.elkheer.R
import com.ebshaway.elkheer.model.Group

class GroupsAdapter(
    private var runSheetModels: ArrayList<Group>?,
    private var listener: itemClicked
) :
    RecyclerView.Adapter<GroupsAdapter.ViewHolder>() {
    private var row_index = -1
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.group_item, parent, false)

        return ViewHolder(itemView)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = runSheetModels!![position]

        holder.container.setOnClickListener {
            listener.onClick(item)
        }

        holder.name?.text = item.name
        holder.image?.setImageDrawable(
            ContextCompat.getDrawable(
                holder.itemView.context,
                item.image
            )
        )
    }

    override fun getItemCount(): Int {
        return runSheetModels!!.size
    }


    inner class ViewHolder(convertView: View) : RecyclerView.ViewHolder(convertView) {
        //group
        var name: TextView? = convertView.findViewById(R.id.group_text)
        var image: ImageView? = convertView.findViewById(R.id.group_image)
        var container: ConstraintLayout = convertView.findViewById(R.id.container)
    }

    interface itemClicked {
        fun onClick(item: Group)
    }
}