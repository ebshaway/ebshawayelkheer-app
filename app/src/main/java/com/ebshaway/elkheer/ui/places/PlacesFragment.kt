package com.ebshaway.elkheer.ui.places

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager

import com.ebshaway.elkheer.R
import com.ebshaway.elkheer.helpers.FirestoreHelper
import com.ebshaway.elkheer.model.GroupDetails
import com.ebshaway.elkheer.model.Place
import com.google.gson.Gson
import kotlinx.android.synthetic.main.app_bar.*
import kotlinx.android.synthetic.main.fragment_places.*


open class PlacesFragment : Fragment(), placesAdapter.itemClicked, FirestoreHelper.getDetails {

    var adapter: placesAdapter? = null
    private var placeItems: ArrayList<Place> = ArrayList()
    private lateinit var progressDialog: Dialog

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_places, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showProgressDialog()
        activity_name.text = resources.getString(R.string.centers)
        arguments?.getInt("groupId")?.let { FirestoreHelper.getGroupDetails(it.toString(), this) }
        val linearLayoutManager = LinearLayoutManager(context)
        places_recycler.layoutManager = linearLayoutManager
        places_recycler.setHasFixedSize(true)
        adapter =
            placesAdapter(placeItems, this)
        places_recycler.adapter = adapter

        back.setOnClickListener {
            view.findNavController().navigate(
                R.id.action_places_to_groups
            )
        }
    }

    override fun onClick(item: Place) {
        view?.findNavController()?.navigate(
            R.id.action_places_to_center, bundleOf(
                "place" to Gson().toJson(item),
                "groupId" to arguments?.getInt("groupId")
            )
        )
    }

    override fun onDetails(data: String) {
        val groupDetails = Gson().fromJson(data, GroupDetails::class.java)
        placeItems.clear()
        groupDetails.places?.let { placeItems.addAll(it) }
        adapter!!.notifyDataSetChanged()
        hideProgressDialog()
    }

    private fun showProgressDialog() {
        hideProgressDialog()
        val alertDialogBuilder = AlertDialog.Builder(activity).setCancelable(false)
        alertDialogBuilder.setView(R.layout.dialog_prog)
        progressDialog = alertDialogBuilder.create()
        progressDialog.window?.setBackgroundDrawableResource(R.color.transparent)
        progressDialog.show()
    }

    private fun hideProgressDialog() {
        if (::progressDialog.isInitialized && progressDialog.isShowing)
            progressDialog.dismiss()
    }
}
