package com.ebshaway.elkheer.ui.main

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import com.ebshaway.elkheer.R
import com.ebshaway.elkheer.helpers.FirestoreHelper

class SplashActivity : AppCompatActivity() {

    private val secondsDelayed = 2

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        handleProceed()
    }

    private fun handleProceed() {
        Handler().postDelayed(
            {
               var intent= Intent(this@SplashActivity, MainActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
            },
            (secondsDelayed * 2000).toLong()
        )
    }
}
