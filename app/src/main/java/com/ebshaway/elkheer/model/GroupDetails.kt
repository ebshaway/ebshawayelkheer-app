package com.ebshaway.elkheer.model

class GroupDetails {
    constructor()
    constructor(
        id: Long,
        name: String,
        image: String,
        places: List<Place>?
    ) {
        this.id = id
        this.name = name
        this.image = image
        this.places = places

    }

    var id: Long? = null
    var name: String? = null
    var image: String? = null
    var places: List<Place>? = null
    override fun toString(): String {
        return "GroupDetails(id=$id, name=$name, image=$image, places=$places)"
    }


}