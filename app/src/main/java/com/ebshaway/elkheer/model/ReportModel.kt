package com.ebshaway.elkheer.model

data class ReportModel (
    var name: String,
    var address: String,
    var phone: String,
    var status: String,
    var type: String,
    var message: String
)