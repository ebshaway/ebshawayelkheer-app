package com.ebshaway.elkheer.model

class Place {
    constructor()
    constructor(
        name: String,
        address: String,
        phone: String,
        icon: Int,
        whatsapp: String
    ) {
        this.name = name
        this.address = address
        this.phone = phone
        this.whatsapp = whatsapp
        this.type = icon
    }

    var name: String? = null
    var address: String? = null
    var phone: String? = null
    var whatsapp: String? = null
    var type: Int? = null


}