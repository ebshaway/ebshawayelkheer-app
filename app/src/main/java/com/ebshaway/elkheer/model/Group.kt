package com.ebshaway.elkheer.model

data class Group(
    var id: Int,
    var name: String,
    var image: Int
)